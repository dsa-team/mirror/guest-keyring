export JETRING_SIGN=$(shell if [ -e dsa.gpg ]; then echo dsa.gpg; else echo "** Warning: Generating keyring without checking signatures!" >&2 ; fi)
export PATH=/usr/local/bin:/usr/bin:/bin

all: dsa.gpg debian-guest.gpg

debian-guest.gpg: debian-guest/index
	jetring-build -I $@ debian-guest

dsa.gpg: dsa/index
	jetring-build -I $@ dsa

clean:
	rm -f debian-guest.gpg* dsa.gpg.*
